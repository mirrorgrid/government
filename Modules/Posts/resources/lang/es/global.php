<?php

return [
    'name'     => 'Objetos',
    'posts'  => 'objeto|objetos',
    'New'      => 'Nuevo objeto',
    'Edit'     => 'Editar objeto',
    'Back'     => 'Volver a los objetos',
];
