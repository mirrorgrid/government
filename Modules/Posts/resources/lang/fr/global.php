<?php

return [
    'name'     => 'Posts',
    'posts'  => 'post|posts',
    'New'      => 'Nouveau post',
    'Edit'     => 'Modifier post',
    'Back'     => 'Retour à la liste des posts',
];
