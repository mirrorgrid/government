<?php

return [
    'name'     => 'Posts',
    'posts'  => 'post|posts',
    'New'      => 'New post',
    'Edit'     => 'Edit post',
    'Back'     => 'Back to posts',
];
