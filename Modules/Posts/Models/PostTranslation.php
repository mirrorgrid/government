<?php

namespace TypiCMS\Modules\Posts\Models;

use TypiCMS\Modules\Core\Models\BaseTranslation;

class PostTranslation extends BaseTranslation
{
    protected $fillable = [
        'title',
        'slug',
        'status',
        'summary',
        'body',
    ];

    /**
     * get the parent model.
     */
    public function owner()
    {
        return $this->belongsTo('TypiCMS\Modules\Posts\Models\Post', 'post_id');
    }
}
