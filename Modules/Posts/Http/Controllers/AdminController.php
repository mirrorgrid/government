<?php

namespace TypiCMS\Modules\Posts\Http\Controllers;

use TypiCMS\Modules\Core\Http\Controllers\BaseAdminController;
use TypiCMS\Modules\Posts\Http\Requests\FormRequest;
use TypiCMS\Modules\Posts\Models\Post;
use TypiCMS\Modules\Posts\Repositories\PostInterface;

class AdminController extends BaseAdminController
{
    public function __construct(PostInterface $post)
    {
        parent::__construct($post);
    }

    /**
     * List models.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $models = $this->repository->all([], true);
        app('JavaScript')->put('models', $models);

        return view('posts::admin.index');
    }

    /**
     * Create form for a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $model = $this->repository->getModel();

        return view('posts::admin.create')
            ->with(compact('model'));
    }

    /**
     * Edit form for the specified resource.
     *
     * @param \TypiCMS\Modules\Posts\Models\Post $post
     *
     * @return \Illuminate\View\View
     */
    public function edit(Post $post)
    {
        return view('posts::admin.edit')
            ->with(['model' => $post]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \TypiCMS\Modules\Posts\Http\Requests\FormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FormRequest $request)
    {
        $post = $this->repository->create($request->all());

        return $this->redirect($request, $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \TypiCMS\Modules\Posts\Models\Post             $post
     * @param \TypiCMS\Modules\Posts\Http\Requests\FormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Post $post, FormRequest $request)
    {
        $this->repository->update($request->all());

        return $this->redirect($request, $post);
    }
}
