<?php

namespace TypiCMS\Modules\Posts\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use TypiCMS\Modules\Core\Facades\TypiCMS;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'TypiCMS\Modules\Posts\Http\Controllers';

    /**
     * Define the routes for the application.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function (Router $router) {

            /*
             * Front office routes
             */
            if ($page = TypiCMS::getPageLinkedToModule('posts')) {
                $options = $page->private ? ['middleware' => 'auth'] : [];
                foreach (config('translatable.locales') as $lang) {
                    if ($page->translate($lang)->status && $uri = $page->uri($lang)) {
                        $router->get($uri, $options + ['as' => $lang.'.posts', 'uses' => 'PublicController@index']);
                        $router->get($uri.'/{slug}', $options + ['as' => $lang.'.posts.slug', 'uses' => 'PublicController@show']);
                    }
                }
            }

            /*
             * Admin routes
             */
            $router->get('admin/posts', 'AdminController@index')->name('admin::index-posts');
            $router->get('admin/posts/create', 'AdminController@create')->name('admin::create-post');
            $router->get('admin/posts/{post}/edit', 'AdminController@edit')->name('admin::edit-post');
            $router->post('admin/posts', 'AdminController@store')->name('admin::store-post');
            $router->put('admin/posts/{post}', 'AdminController@update')->name('admin::update-post');

            /*
             * API routes
             */
            $router->get('api/posts', 'ApiController@index')->name('api::index-posts');
            $router->put('api/posts/{post}', 'ApiController@update')->name('api::update-post');
            $router->delete('api/posts/{post}', 'ApiController@destroy')->name('api::destroy-post');
        });
    }
}
