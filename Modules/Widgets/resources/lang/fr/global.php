<?php

return [
    'name'     => 'Widgets',
    'widgets'  => 'widget|widgets',
    'New'      => 'Nouveau widget',
    'Edit'     => 'Modifier widget',
    'Back'     => 'Retour à la liste des widgets',
];
