<?php

namespace TypiCMS\Modules\Widgets\Repositories;

use Illuminate\Database\Eloquent\Model;
use TypiCMS\Modules\Core\Repositories\RepositoriesAbstract;

class EloquentWidget extends RepositoriesAbstract implements WidgetInterface
{
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}
