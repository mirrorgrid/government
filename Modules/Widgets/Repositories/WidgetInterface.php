<?php

namespace TypiCMS\Modules\Widgets\Repositories;

use TypiCMS\Modules\Core\Repositories\RepositoryInterface;

interface WidgetInterface extends RepositoryInterface
{
}
