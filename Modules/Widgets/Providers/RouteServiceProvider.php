<?php

namespace TypiCMS\Modules\Widgets\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use TypiCMS\Modules\Core\Facades\TypiCMS;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'TypiCMS\Modules\Widgets\Http\Controllers';

    /**
     * Define the routes for the application.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function (Router $router) {

            /*
             * Front office routes
             */
            if ($page = TypiCMS::getPageLinkedToModule('widgets')) {
                $options = $page->private ? ['middleware' => 'auth'] : [];
                foreach (config('translatable.locales') as $lang) {
                    if ($page->translate($lang)->status && $uri = $page->uri($lang)) {
                        $router->get($uri, $options + ['as' => $lang.'.widgets', 'uses' => 'PublicController@index']);
                        $router->get($uri.'/{slug}', $options + ['as' => $lang.'.widgets.slug', 'uses' => 'PublicController@show']);
                    }
                }
            }

            /*
             * Admin routes
             */
            $router->get('admin/widgets', 'AdminController@index')->name('admin::index-widgets');
            $router->get('admin/widgets/create', 'AdminController@create')->name('admin::create-widget');
            $router->get('admin/widgets/{widget}/edit', 'AdminController@edit')->name('admin::edit-widget');
            $router->post('admin/widgets', 'AdminController@store')->name('admin::store-widget');
            $router->put('admin/widgets/{widget}', 'AdminController@update')->name('admin::update-widget');

            /*
             * API routes
             */
            $router->get('api/widgets', 'ApiController@index')->name('api::index-widgets');
            $router->put('api/widgets/{widget}', 'ApiController@update')->name('api::update-widget');
            $router->delete('api/widgets/{widget}', 'ApiController@destroy')->name('api::destroy-widget');
        });
    }
}
