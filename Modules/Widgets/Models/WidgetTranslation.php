<?php

namespace TypiCMS\Modules\Widgets\Models;

use TypiCMS\Modules\Core\Models\BaseTranslation;

class WidgetTranslation extends BaseTranslation
{
    protected $fillable = [
        'title',
        'slug',
        'status',
        'summary',
        'body',
    ];

    /**
     * get the parent model.
     */
    public function owner()
    {
        return $this->belongsTo('TypiCMS\Modules\Widgets\Models\Widget', 'widget_id');
    }
}
