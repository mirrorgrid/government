<?php

namespace TypiCMS\Modules\Widgets\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Gate;
use Maatwebsite\Sidebar\SidebarGroup;
use Maatwebsite\Sidebar\SidebarItem;

class SidebarViewComposer
{
    public function compose(View $view)
    {
        $view->sidebar->group(trans('global.menus.content'), function (SidebarGroup $group) {
            $group->addItem(trans('widgets::global.name'), function (SidebarItem $item) {
                $item->id = 'widgets';
                $item->icon = config('typicms.widgets.sidebar.icon');
                $item->weight = config('typicms.widgets.sidebar.weight');
                $item->route('admin::index-widgets');
                $item->append('admin::create-widget');
                $item->authorize(
                    Gate::allows('index-widgets')
                );
            });
        });
    }
}
