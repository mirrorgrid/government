-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 26, 2017 at 12:45 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `government`
--

-- --------------------------------------------------------

--
-- Table structure for table `typicms_blocks`
--

CREATE TABLE `typicms_blocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_blocks`
--

INSERT INTO `typicms_blocks` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'footer-4-block', '2017-11-19 11:30:33', '2017-11-19 20:29:29'),
(2, 'footer-3-block', '2017-11-23 05:15:00', '2017-11-23 05:16:07');

-- --------------------------------------------------------

--
-- Table structure for table `typicms_block_translations`
--

CREATE TABLE `typicms_block_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `block_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_block_translations`
--

INSERT INTO `typicms_block_translations` (`id`, `block_id`, `locale`, `status`, `body`, `created_at`, `updated_at`) VALUES
(1, 1, 'en', 1, '<p>New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish New block Save and exitSaveEnglish</p>\r\n', '2017-11-19 11:30:34', '2017-11-19 20:27:57'),
(2, 1, 'np', 1, '<p>धनगढी उप-महानगरपालिका धनगढी कैलाली&nbsp;</p>\r\n\r\n<p>फोन नं: +९७७ ०९१-५२१५२९ फ्याकस:&nbsp;: +९७७ ०९१-५२१५२९&nbsp;</p>\r\n\r\n<p>इ-मेल:&nbsp;<a href="mailto:info@dhangadhimun.gov.np">info@dhangadhimun.gov.np</a><a href="mailto:dhangadhimun2013@gmail.com">dhangadhimun2013@gmail.com</a>&nbsp;</p>\r\n\r\n<p>वेबसाइट:&nbsp;<a href="http://www.dhangadhimun.gov.np/">www.dhangadhimun.gov.np</a></p>\r\n', '2017-11-19 11:30:34', '2017-11-19 20:29:29'),
(3, 2, 'en', 0, '', '2017-11-23 05:15:01', '2017-11-23 05:15:01'),
(4, 2, 'np', 1, '<div class="block block-menu" id="block-menu-menu-reports">\r\n<h2>प्रतिवेदन</h2>\r\n\r\n<div class="content">\r\n<ul class="menu clearfix">\r\n	<li class="first last leaf"><a href="/ne/annual_progress" title="">वार्षिक प्रगति समिक्षा</a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n', '2017-11-23 05:15:01', '2017-11-23 05:16:06');

-- --------------------------------------------------------

--
-- Table structure for table `typicms_files`
--

CREATE TABLE `typicms_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `gallery_id` int(10) UNSIGNED DEFAULT NULL,
  `type` enum('a','v','d','i','o') COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `extension` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mimetype` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `width` int(10) UNSIGNED DEFAULT NULL,
  `height` int(10) UNSIGNED DEFAULT NULL,
  `filesize` int(10) UNSIGNED DEFAULT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_files`
--

INSERT INTO `typicms_files` (`id`, `gallery_id`, `type`, `file`, `path`, `extension`, `mimetype`, `width`, `height`, `filesize`, `position`, `created_at`, `updated_at`) VALUES
(1, 1, 'i', 'screen.PNG', 'uploads/files', '.PNG', 'image/png', 528, 570, 350581, 0, '2017-11-19 07:19:23', '2017-11-19 07:19:23'),
(2, 1, 'i', 'capture.PNG', 'uploads/files', '.PNG', 'image/png', 901, 650, 540019, 0, '2017-11-19 07:19:28', '2017-11-19 07:19:28'),
(3, 1, 'i', 'screen_1.PNG', 'uploads/files', '.PNG', 'image/png', 528, 570, 350581, 0, '2017-11-19 07:19:33', '2017-11-19 07:19:33'),
(4, 1, 'i', 'jquery-logo.png', 'uploads/files', '.png', 'image/png', 523, 192, 12570, 0, '2017-11-19 07:19:36', '2017-11-19 07:19:36');

-- --------------------------------------------------------

--
-- Table structure for table `typicms_file_translations`
--

CREATE TABLE `typicms_file_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `file_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `alt_attribute` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `typicms_galleries`
--

CREATE TABLE `typicms_galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_galleries`
--

INSERT INTO `typicms_galleries` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(1, 'test-content', '01-homeblue.jpg', '2017-11-19 07:18:22', '2017-11-19 20:26:14');

-- --------------------------------------------------------

--
-- Table structure for table `typicms_galleryables`
--

CREATE TABLE `typicms_galleryables` (
  `id` int(10) UNSIGNED NOT NULL,
  `gallery_id` int(10) UNSIGNED NOT NULL,
  `galleryable_id` int(10) UNSIGNED NOT NULL,
  `galleryable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_galleryables`
--

INSERT INTO `typicms_galleryables` (`id`, `gallery_id`, `galleryable_id`, `galleryable_type`, `position`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'TypiCMS\\Modules\\Pages\\Models\\Page', 0, '2017-11-19 20:23:01', '2017-11-19 20:23:01');

-- --------------------------------------------------------

--
-- Table structure for table `typicms_gallery_translations`
--

CREATE TABLE `typicms_gallery_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `gallery_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `summary` text COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_gallery_translations`
--

INSERT INTO `typicms_gallery_translations` (`id`, `gallery_id`, `locale`, `status`, `title`, `slug`, `summary`, `body`, `created_at`, `updated_at`) VALUES
(1, 1, 'en', 1, 'Test En Titile', 'test-en-titile', 'This is test en content', '<p>This is body en&nbsp;</p>\r\n', '2017-11-19 07:18:22', '2017-11-19 07:18:22'),
(2, 1, 'np', 1, 'नेपाली कन्टेन्ट', 'nepali-test-content', 'This is nepali content for test :) ', '<p>Thi is nepali test content :)&nbsp;</p>\r\n', '2017-11-19 07:18:22', '2017-11-19 07:19:11');

-- --------------------------------------------------------

--
-- Table structure for table `typicms_history`
--

CREATE TABLE `typicms_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `historable_id` int(10) UNSIGNED NOT NULL,
  `historable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `historable_table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_history`
--

INSERT INTO `typicms_history` (`id`, `historable_id`, `historable_type`, `user_id`, `title`, `icon_class`, `locale`, `historable_table`, `action`, `created_at`, `updated_at`) VALUES
(99, 8, 'TypiCMS\\Modules\\Menus\\Models\\Menulink', 1, 'Hello World', 'fa-edit', NULL, 'menulinks', 'updated', '2017-11-22 07:41:18', '2017-11-22 07:41:18'),
(100, 7, 'TypiCMS\\Modules\\Menus\\Models\\Menulink', 1, 'Test', 'fa-edit', NULL, 'menulinks', 'updated', '2017-11-23 04:37:55', '2017-11-23 04:37:55'),
(101, 9, 'TypiCMS\\Modules\\Menus\\Models\\Menulink', 1, 'Footer Menu 1', 'fa-plus-circle', NULL, 'menulinks', 'created', '2017-11-23 04:40:27', '2017-11-23 04:40:27'),
(102, 9, 'TypiCMS\\Modules\\Menus\\Models\\Menulink', 1, 'Footer Menu 1', 'fa-edit', NULL, 'menulinks', 'updated', '2017-11-23 04:40:47', '2017-11-23 04:40:47'),
(103, 2, 'TypiCMS\\Modules\\Menus\\Models\\Menulink', 1, 'कन्ट्याक्ट', 'fa-edit', NULL, 'menulinks', 'updated', '2017-11-23 05:09:05', '2017-11-23 05:09:05'),
(104, 1, 'TypiCMS\\Modules\\Menus\\Models\\Menulink', 1, 'गृहकाे टाइटल ', 'fa-edit', NULL, 'menulinks', 'updated', '2017-11-23 05:09:05', '2017-11-23 05:09:05'),
(105, 1, 'TypiCMS\\Modules\\Menus\\Models\\Menu', 1, 'main', 'fa-edit', NULL, 'menus', 'updated', '2017-11-23 05:09:08', '2017-11-23 05:09:08'),
(106, 1, 'TypiCMS\\Modules\\Menus\\Models\\Menulink', 1, 'गृहकाे टाइटल ', 'fa-edit', NULL, 'menulinks', 'updated', '2017-11-23 05:09:46', '2017-11-23 05:09:46'),
(107, 1, 'TypiCMS\\Modules\\Menus\\Models\\Menu', 1, 'main', 'fa-edit', NULL, 'menus', 'updated', '2017-11-23 05:09:47', '2017-11-23 05:09:47'),
(108, 2, 'TypiCMS\\Modules\\Blocks\\Models\\Block', 1, 'footer-3-block', 'fa-plus-circle', NULL, 'blocks', 'created', '2017-11-23 05:15:01', '2017-11-23 05:15:01'),
(109, 2, 'TypiCMS\\Modules\\Blocks\\Models\\Block', 1, 'footer-3-block', 'fa-toggle-on', 'np', 'blocks', 'set online', '2017-11-23 05:16:07', '2017-11-23 05:16:07'),
(110, 2, 'TypiCMS\\Modules\\Blocks\\Models\\Block', 1, 'footer-3-block', 'fa-edit', NULL, 'blocks', 'updated', '2017-11-23 05:16:07', '2017-11-23 05:16:07'),
(111, 5, 'TypiCMS\\Modules\\Menus\\Models\\Menu', 1, 'footer-3', 'fa-toggle-on', 'np', 'menus', 'set online', '2017-11-23 05:16:30', '2017-11-23 05:16:30'),
(112, 5, 'TypiCMS\\Modules\\Menus\\Models\\Menu', 1, 'footer-3', 'fa-edit', NULL, 'menus', 'updated', '2017-11-23 05:16:30', '2017-11-23 05:16:30'),
(113, 6, 'TypiCMS\\Modules\\Menus\\Models\\Menu', 1, 'footer-4', 'fa-trash', NULL, 'menus', 'deleted', '2017-11-23 05:16:32', '2017-11-23 05:16:32'),
(114, 2, 'TypiCMS\\Modules\\Pages\\Models\\Page', 1, 'Contact', 'fa-edit', NULL, 'pages', 'updated', '2017-11-23 05:19:07', '2017-11-23 05:19:07'),
(115, 2, 'TypiCMS\\Modules\\Pages\\Models\\Page', 1, 'Contact', 'fa-edit', 'en', 'pages', 'updated', '2017-11-23 05:19:07', '2017-11-23 05:19:07'),
(116, 2, 'TypiCMS\\Modules\\Pages\\Models\\Page', 1, 'Contact', 'fa-edit', 'np', 'pages', 'updated', '2017-11-23 05:19:08', '2017-11-23 05:19:08'),
(117, 3, 'TypiCMS\\Modules\\Pages\\Models\\Page', 1, 'Terms of use', 'fa-edit', NULL, 'pages', 'updated', '2017-11-23 05:19:09', '2017-11-23 05:19:09'),
(118, 3, 'TypiCMS\\Modules\\Pages\\Models\\Page', 1, 'Terms of use', 'fa-edit', 'en', 'pages', 'updated', '2017-11-23 05:19:10', '2017-11-23 05:19:10'),
(119, 3, 'TypiCMS\\Modules\\Pages\\Models\\Page', 1, 'Terms of use', 'fa-edit', 'np', 'pages', 'updated', '2017-11-23 05:19:10', '2017-11-23 05:19:10'),
(120, 1, 'TypiCMS\\Modules\\Widgets\\Models\\Widget', 1, 'Rigth Widget', 'fa-plus-circle', NULL, 'widgets', 'created', '2017-11-23 05:21:01', '2017-11-23 05:21:01'),
(121, 1, 'TypiCMS\\Modules\\Widgets\\Models\\Widget', 1, '', 'fa-toggle-on', 'en', 'widgets', 'set online', '2017-11-23 05:21:13', '2017-11-23 05:21:13'),
(122, 1, 'TypiCMS\\Modules\\Widgets\\Models\\Widget', 1, '', 'fa-edit', NULL, 'widgets', 'updated', '2017-11-23 05:21:13', '2017-11-23 05:21:13'),
(123, 1, 'TypiCMS\\Modules\\Widgets\\Models\\Widget', 1, 'This is Untitled', 'fa-edit', 'en', 'widgets', 'updated', '2017-11-23 05:21:37', '2017-11-23 05:21:37'),
(124, 1, 'TypiCMS\\Modules\\Widgets\\Models\\Widget', 1, 'This is Untitled', 'fa-edit', NULL, 'widgets', 'updated', '2017-11-23 05:21:37', '2017-11-23 05:21:37'),
(125, 1, 'TypiCMS\\Modules\\Posts\\Models\\Post', 1, 'पाेस्ट टाइट', 'fa-edit', NULL, 'posts', 'updated', '2017-11-23 08:54:40', '2017-11-23 08:54:40'),
(126, 1, 'TypiCMS\\Modules\\Posts\\Models\\Post', 1, 'पाेस्ट टाइट', 'fa-edit', NULL, 'posts', 'updated', '2017-11-23 08:55:58', '2017-11-23 08:55:58'),
(127, 1, 'TypiCMS\\Modules\\Widgets\\Models\\Widget', 1, 'Rigth Widget', 'fa-toggle-on', 'np', 'widgets', 'set online', '2017-11-23 08:58:40', '2017-11-23 08:58:40'),
(128, 1, 'TypiCMS\\Modules\\Widgets\\Models\\Widget', 1, 'Rigth Widget', 'fa-edit', NULL, 'widgets', 'updated', '2017-11-23 08:58:40', '2017-11-23 08:58:40'),
(129, 1, 'TypiCMS\\Modules\\Widgets\\Models\\Widget', 1, 'दायाँ विजेट', 'fa-edit', 'np', 'widgets', 'updated', '2017-11-23 08:59:02', '2017-11-23 08:59:02'),
(130, 1, 'TypiCMS\\Modules\\Widgets\\Models\\Widget', 1, 'दायाँ विजेट', 'fa-edit', NULL, 'widgets', 'updated', '2017-11-23 08:59:02', '2017-11-23 08:59:02'),
(131, 1, 'TypiCMS\\Modules\\Widgets\\Models\\Widget', 1, 'This is Untitled', 'fa-edit', NULL, 'widgets', 'updated', '2017-11-23 08:59:18', '2017-11-23 08:59:18'),
(132, 1, 'TypiCMS\\Modules\\Widgets\\Models\\Widget', 1, 'This is Untitled', 'fa-edit', NULL, 'widgets', 'updated', '2017-11-23 09:00:00', '2017-11-23 09:00:00'),
(133, 2, 'TypiCMS\\Modules\\Widgets\\Models\\Widget', 1, 'Another Widgets', 'fa-plus-circle', NULL, 'widgets', 'created', '2017-11-23 09:00:59', '2017-11-23 09:00:59'),
(134, 1, 'TypiCMS\\Modules\\Widgets\\Models\\Widget', 1, 'This is Untitled', 'fa-edit', NULL, 'widgets', 'updated', '2017-11-23 09:20:56', '2017-11-23 09:20:56'),
(135, 1, 'TypiCMS\\Modules\\Widgets\\Models\\Widget', 1, 'This is Untitled', 'fa-edit', NULL, 'widgets', 'updated', '2017-11-23 09:44:24', '2017-11-23 09:44:24');

-- --------------------------------------------------------

--
-- Table structure for table `typicms_menulinks`
--

CREATE TABLE `typicms_menulinks` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `target` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `has_categories` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_menulinks`
--

INSERT INTO `typicms_menulinks` (`id`, `menu_id`, `page_id`, `parent_id`, `position`, `target`, `class`, `icon_class`, `has_categories`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, 1, '_blank', '', '', 0, '2014-03-28 16:23:05', '2017-11-23 05:09:45'),
(2, 1, 2, NULL, 1, '', '', '', 0, '2014-03-28 17:33:49', '2017-11-23 05:09:04'),
(5, 3, 0, NULL, 1, '_blank', 'btn-facebook', 'fa fa-facebook fa-fw', 0, '2014-03-28 12:45:11', '2014-03-28 12:45:17'),
(6, 3, 0, NULL, 2, '_blank', 'btn-twitter', 'fa fa-twitter fa-fw', 0, '2014-03-28 12:46:35', '2014-03-28 12:46:47'),
(7, 4, 2, 5, 0, '_blank', '', '', 0, '2017-11-19 10:14:30', '2017-11-23 04:37:55'),
(8, 4, 2, NULL, 0, '_blank', 'hello-calss', 'class', 1, '2017-11-19 10:21:33', '2017-11-22 07:41:17'),
(9, 4, 3, 2, 0, '', '', '', NULL, '2017-11-23 04:40:27', '2017-11-23 04:40:47');

-- --------------------------------------------------------

--
-- Table structure for table `typicms_menulink_translations`
--

CREATE TABLE `typicms_menulink_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `menulink_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_menulink_translations`
--

INSERT INTO `typicms_menulink_translations` (`id`, `menulink_id`, `locale`, `status`, `title`, `url`, `created_at`, `updated_at`) VALUES
(1, 1, 'fr', 1, 'Accueil', '', '2014-03-27 18:15:00', '2014-03-27 18:15:00'),
(2, 1, 'nl', 1, 'Home', '', '2014-03-27 18:15:00', '2014-03-27 18:15:00'),
(3, 1, 'en', 1, 'Home', '', '2014-03-27 18:15:00', '2014-03-27 18:15:00'),
(4, 1, 'es', 1, 'Inicio', '', '2014-03-27 18:15:00', '2014-03-27 18:15:00'),
(5, 2, 'fr', 1, 'Contact', '', '2014-03-27 18:15:00', '2014-03-28 07:44:27'),
(6, 2, 'nl', 1, 'Contact', '', '2014-03-27 18:15:00', '2014-03-28 07:44:27'),
(7, 2, 'en', 1, 'Contact', '', '2014-03-27 18:15:00', '2014-03-28 07:44:27'),
(8, 2, 'es', 1, 'Contacto', '', '2014-03-27 18:15:00', '2014-03-28 07:44:27'),
(17, 5, 'fr', 1, 'Facebook', 'https://www.facebook.com', '2014-03-28 12:45:11', '2014-03-28 12:45:17'),
(18, 5, 'nl', 1, 'Facebook', 'https://www.facebook.com', '2014-03-28 12:45:11', '2014-03-28 12:45:17'),
(19, 5, 'en', 1, 'Facebook', 'https://www.facebook.com', '2014-03-28 12:45:11', '2014-03-28 12:45:17'),
(20, 5, 'es', 1, 'Facebook', 'https://www.facebook.com', '2014-03-28 12:45:11', '2014-03-28 12:45:17'),
(21, 6, 'fr', 1, 'Twitter', 'https://twitter.com', '2014-03-28 12:46:35', '2014-03-28 12:46:47'),
(22, 6, 'nl', 1, 'Twitter', 'https://twitter.com', '2014-03-28 12:46:35', '2014-03-28 12:46:44'),
(23, 6, 'en', 1, 'Twitter', 'https://twitter.com', '2014-03-28 12:46:35', '2014-03-28 12:46:42'),
(24, 6, 'es', 1, 'Twitter', 'https://twitter.com', '2014-03-28 12:46:35', '2014-03-28 12:46:47'),
(27, 1, 'np', 1, 'गृहकाे टाइटल ', '', '2017-11-19 03:20:34', '2017-11-19 03:20:52'),
(28, 2, 'np', 1, 'कन्ट्याक्ट', '', '2017-11-19 03:21:54', '2017-11-19 03:22:08'),
(29, 7, 'en', 1, 'Test', '', '2017-11-19 10:14:30', '2017-11-19 10:17:54'),
(30, 7, 'np', 1, 'टेस्ट लिङ्क', '', '2017-11-19 10:14:31', '2017-11-19 10:43:34'),
(31, 8, 'en', 1, 'Hello World', '', '2017-11-19 10:21:33', '2017-11-19 10:21:33'),
(32, 8, 'np', 1, 'अर्काे टाइटल', '', '2017-11-19 10:21:34', '2017-11-19 10:44:45'),
(33, 9, 'en', 0, 'Footer Menu 1', 'http://thisisfootermenu.com', '2017-11-23 04:40:27', '2017-11-23 04:40:27'),
(34, 9, 'np', 0, '', '', '2017-11-23 04:40:27', '2017-11-23 04:40:27');

-- --------------------------------------------------------

--
-- Table structure for table `typicms_menus`
--

CREATE TABLE `typicms_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_menus`
--

INSERT INTO `typicms_menus` (`id`, `name`, `class`, `created_at`, `updated_at`) VALUES
(1, 'main', '', '2013-09-03 16:20:21', '2017-11-18 08:38:20'),
(3, 'social', '', '2014-02-04 12:42:18', '2017-11-18 08:38:21'),
(4, 'footer-1', 'footer-1', '2017-11-19 10:09:08', '2017-11-19 10:38:03'),
(5, 'footer-3', 'footer-3', '2017-11-19 10:09:25', '2017-11-23 05:16:30');

-- --------------------------------------------------------

--
-- Table structure for table `typicms_menu_translations`
--

CREATE TABLE `typicms_menu_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_menu_translations`
--

INSERT INTO `typicms_menu_translations` (`id`, `menu_id`, `locale`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'fr', 1, '2014-02-17 10:15:00', '2014-02-17 10:15:00'),
(2, 1, 'nl', 1, '2014-02-17 10:15:00', '2014-02-17 10:15:00'),
(3, 1, 'en', 1, '2014-02-17 10:15:00', '2014-02-17 10:15:00'),
(4, 1, 'es', 1, '2014-02-17 10:15:00', '2014-02-17 10:15:00'),
(9, 3, 'fr', 1, '2014-02-04 12:42:18', '2014-02-04 12:50:01'),
(10, 3, 'nl', 1, '2014-02-04 12:42:18', '2014-02-04 12:50:01'),
(11, 3, 'en', 1, '2014-02-04 12:42:18', '2014-02-04 12:50:01'),
(12, 3, 'es', 1, '2014-02-04 12:42:18', '2014-02-04 12:50:01'),
(14, 1, 'np', 1, '2017-11-18 08:38:20', '2017-11-18 08:38:20'),
(15, 3, 'np', 1, '2017-11-18 08:38:21', '2017-11-18 08:38:21'),
(16, 4, 'en', 1, '2017-11-19 10:09:08', '2017-11-19 10:36:50'),
(17, 4, 'np', 1, '2017-11-19 10:09:09', '2017-11-19 10:38:03'),
(18, 5, 'en', 1, '2017-11-19 10:09:25', '2017-11-19 10:36:57'),
(19, 5, 'np', 1, '2017-11-19 10:09:25', '2017-11-23 05:16:30');

-- --------------------------------------------------------

--
-- Table structure for table `typicms_migrations`
--

CREATE TABLE `typicms_migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_migrations`
--

INSERT INTO `typicms_migrations` (`migration`, `batch`) VALUES
('2012_12_06_225921_create_users_table', 1),
('2012_12_06_225922_create_password_resets_table', 1),
('2013_08_29_174626_create_pages_table', 1),
('2013_09_03_084147_create_menus_tables', 1),
('2013_09_03_084148_create_menulinks_tables', 1),
('2013_10_29_224632_create_settings_table', 1),
('2014_02_28_223553_create_translations_table', 1),
('2014_05_09_095101_create_galleries_table', 1),
('2014_05_09_110000_create_files_table', 1),
('2014_05_28_103804_create_galleryable_table', 1),
('2014_06_19_090602_create_blocks_table', 1),
('2014_11_03_151402_create_history_table', 1),
('2017_11_18_134855_create_permission_tables', 1),
('2017_11_20_021639_create_news_table', 2),
('2017_11_20_134127_create_posts_table', 2),
('2017_11_20_135330_create_widgets_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `typicms_news`
--

CREATE TABLE `typicms_news` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `typicms_news_translations`
--

CREATE TABLE `typicms_news_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `news_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `summary` text COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `typicms_pages`
--

CREATE TABLE `typicms_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `meta_robots_no_index` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `meta_robots_no_follow` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `private` tinyint(1) NOT NULL DEFAULT '0',
  `is_home` tinyint(1) NOT NULL DEFAULT '0',
  `redirect` tinyint(1) NOT NULL DEFAULT '0',
  `no_cache` tinyint(1) NOT NULL DEFAULT '0',
  `css` text COLLATE utf8_unicode_ci,
  `js` text COLLATE utf8_unicode_ci,
  `module` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_pages`
--

INSERT INTO `typicms_pages` (`id`, `meta_robots_no_index`, `meta_robots_no_follow`, `image`, `position`, `parent_id`, `private`, `is_home`, `redirect`, `no_cache`, `css`, `js`, `module`, `template`, `created_at`, `updated_at`) VALUES
(1, '', '', NULL, 0, NULL, 0, 1, 0, 0, '', '', '', 'home', '2014-03-28 16:12:44', '2017-11-19 20:23:02'),
(2, '', '', NULL, 1, 1, 0, 0, 0, 0, '', '', '', '', '2014-03-28 16:07:13', '2017-11-23 05:19:08'),
(3, '', '', NULL, 1, 2, 0, 0, 0, 0, '', '', '', '', '2014-03-28 16:13:30', '2017-11-23 05:19:10');

-- --------------------------------------------------------

--
-- Table structure for table `typicms_page_translations`
--

CREATE TABLE `typicms_page_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_page_translations`
--

INSERT INTO `typicms_page_translations` (`id`, `page_id`, `locale`, `slug`, `uri`, `title`, `body`, `status`, `meta_title`, `meta_keywords`, `meta_description`, `created_at`, `updated_at`) VALUES
(1, 1, 'fr', '', NULL, 'Accueil', '<h1>Accueil</h1>', 1, '', '', '', '2014-03-28 10:15:00', '2017-11-18 08:23:56'),
(2, 1, 'nl', '', NULL, 'Home', '<h1>Home</h1>', 1, '', '', '', '2014-03-28 10:15:00', '2014-03-28 10:15:00'),
(3, 1, 'en', '', NULL, 'Home', '<h1>Home</h1>\r\n', 1, '', '', '', '2014-03-28 10:15:00', '2017-11-19 20:23:01'),
(4, 1, 'es', '', NULL, 'Inicio', '<h1>Inicio</h1>', 1, '', '', '', '2014-03-28 10:15:00', '2014-03-28 10:15:00'),
(5, 2, 'fr', 'contact', 'contact', 'Contact', '<h1>Contact</h1>', 1, '', '', '', '2014-03-28 10:15:00', '2014-03-28 10:15:00'),
(6, 2, 'nl', 'contact', 'contact', 'Contact', '<h1>Contact</h1>', 1, '', '', '', '2014-03-28 10:15:00', '2014-03-28 10:15:00'),
(7, 2, 'en', 'contact', 'contact', 'Contact', '<h1>Contact</h1>', 1, '', '', '', '2014-03-28 10:15:00', '2017-11-23 05:19:07'),
(8, 2, 'es', 'contacto', 'contacto', 'Contacto', '<h1>Contacto</h1>', 1, '', '', '', '2014-03-28 10:15:00', '2014-03-28 10:15:00'),
(9, 3, 'fr', 'conditions-d-utilisation', 'conditions-d-utilisation', 'Conditions d’utilisation', '<h1>Conditions d’utilisation</h1>', 1, '', '', '', '2014-03-28 10:15:00', '2014-03-28 10:15:00'),
(10, 3, 'nl', 'gebruiksvoorwaarden', 'gebruiksvoorwaarden', 'Gebruiksvoorwaarden', '<h1>Gebruiksvoorwaarden</h1>', 1, '', '', '', '2014-03-28 10:15:00', '2014-03-28 10:15:00'),
(11, 3, 'en', 'terms-of-use', 'contact/terms-of-use', 'Terms of use', '<h1>Terms of use</h1>', 1, '', '', '', '2014-03-28 10:15:00', '2017-11-23 05:19:09'),
(12, 3, 'es', 'termino-de-uso', 'terminos-de-uso', 'Términos de uso', '<h1>Términos de uso</h1>', 1, '', '', '', '2014-03-28 10:15:00', '2014-03-28 10:15:00'),
(13, 1, 'np', '', NULL, 'गृहकाे टाइटल ', '<h2><a href="http://dhangadhimun.gov.np/ne/node/38">धनगढी उप-महानगरपालिकामा तपाइलाई स्वागत छ l</a></h2>\r\n\r\n<p><strong>धनगढी उप–महानगरपालिकाको एक संक्षिप्त परिचय</strong></p>\r\n\r\n<p>&nbsp;नेपालको सुदूरपश्चिम क्षेत्रमा स्थित धनगढी उप-महानगरपालिकाको स्थापना एउटा दीर्घकालिन सोच&nbsp;<strong>“व्यवसायिक, औद्योगिक तथा बहुसांस्कृतिक समुन्नत शहर धनगढी”</strong>&nbsp;का साथ २०३३ सालमा सेती अञ्चलको कैलाली जिल्लामा भएको हो र मिति २०७२/०६/०१ गते देखि धनगढी नगरपालिकाको स्तरोन्नती भई धनगढी उप–महानगरपालिका हुन गएको हो । यो समुद्री सतहदेखि १०९ मि. को उचाइँमा काठमाडौँ देखि ७५० कि. मि.को दूरीमा स्थित रहेको छ ।</p>\r\n\r\n<p>धनगढी उप–महानगरपालिकामा कूल १९ वडा रहेका छन् । २६१.७५ वर्ग कि.मि.को क्षेत्रफलमा फैलिएको धनगढी उप–महानगरपालिकाको भु–आकृतिक स्वरुप करिब सबै जसो समतल रहेको छ भने यसको पूर्वमा कैलारी गाउँपालिका छ भने पश्चिममा मोहना नदी, उत्तरमा गोदावरी र गौरिगंगा नगरपालिका र दक्षिणमा अन्तर्राष्ट्रिय रुपमा भारतको सिमाना तथा मोहना नदी रहेको छ । यसको शालाखाला अधिकतम तापक्रम ४५ डिग्री र न्यूनतम तापक्रम ७ डिग्री रहेको पाइन्छ ।</p>\r\n\r\n<p>साल २०६८ को तथ्यांक अनुसार यस नगरपालिकाको कुल जनसंख्या १,४७,७४१ रहेको छ |</p>\r\n', 1, NULL, '', '', '2017-11-18 08:23:57', '2017-11-19 20:23:02'),
(14, 2, 'np', 'contac', 'contac', 'Contact', '', 1, NULL, '', '', '2017-11-18 08:24:20', '2017-11-23 05:19:08'),
(15, 3, 'np', 'terms-of-use', 'contac/terms-of-use', 'Terms of use', '', 1, NULL, '', '', '2017-11-18 08:24:41', '2017-11-23 05:19:10');

-- --------------------------------------------------------

--
-- Table structure for table `typicms_password_resets`
--

CREATE TABLE `typicms_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `typicms_permissions`
--

CREATE TABLE `typicms_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_permissions`
--

INSERT INTO `typicms_permissions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'change-locale', NULL, NULL),
(2, 'update-preferences', NULL, NULL),
(3, 'index-history', NULL, NULL),
(4, 'destroy-history', NULL, NULL),
(5, 'dashboard', NULL, NULL),
(6, 'index-settings', NULL, NULL),
(7, 'index-blocks', NULL, NULL),
(8, 'show-block', NULL, NULL),
(9, 'create-block', NULL, NULL),
(10, 'store-block', NULL, NULL),
(11, 'edit-block', NULL, NULL),
(12, 'update-block', NULL, NULL),
(13, 'sort-blocks', NULL, NULL),
(14, 'destroy-block', NULL, NULL),
(15, 'index-files', NULL, NULL),
(16, 'show-file', NULL, NULL),
(17, 'create-file', NULL, NULL),
(18, 'store-file', NULL, NULL),
(19, 'edit-file', NULL, NULL),
(20, 'update-file', NULL, NULL),
(21, 'sort-files', NULL, NULL),
(22, 'destroy-file', NULL, NULL),
(23, 'index-galleries', NULL, NULL),
(24, 'show-gallery', NULL, NULL),
(25, 'create-gallery', NULL, NULL),
(26, 'store-gallery', NULL, NULL),
(27, 'edit-gallery', NULL, NULL),
(28, 'update-gallery', NULL, NULL),
(29, 'sort-galleries', NULL, NULL),
(30, 'destroy-gallery', NULL, NULL),
(31, 'index-menus', NULL, NULL),
(32, 'show-menu', NULL, NULL),
(33, 'create-menu', NULL, NULL),
(34, 'store-menu', NULL, NULL),
(35, 'edit-menu', NULL, NULL),
(36, 'update-menu', NULL, NULL),
(37, 'sort-menus', NULL, NULL),
(38, 'destroy-menu', NULL, NULL),
(39, 'index-pages', NULL, NULL),
(40, 'show-page', NULL, NULL),
(41, 'create-page', NULL, NULL),
(42, 'store-page', NULL, NULL),
(43, 'edit-page', NULL, NULL),
(44, 'update-page', NULL, NULL),
(45, 'sort-pages', NULL, NULL),
(46, 'destroy-page', NULL, NULL),
(47, 'index-roles', NULL, NULL),
(48, 'show-role', NULL, NULL),
(49, 'create-role', NULL, NULL),
(50, 'store-role', NULL, NULL),
(51, 'edit-role', NULL, NULL),
(52, 'update-role', NULL, NULL),
(53, 'sort-roles', NULL, NULL),
(54, 'destroy-role', NULL, NULL),
(55, 'index-translations', NULL, NULL),
(56, 'show-translation', NULL, NULL),
(57, 'create-translation', NULL, NULL),
(58, 'store-translation', NULL, NULL),
(59, 'edit-translation', NULL, NULL),
(60, 'update-translation', NULL, NULL),
(61, 'sort-translations', NULL, NULL),
(62, 'destroy-translation', NULL, NULL),
(63, 'index-users', NULL, NULL),
(64, 'show-user', NULL, NULL),
(65, 'create-user', NULL, NULL),
(66, 'store-user', NULL, NULL),
(67, 'edit-user', NULL, NULL),
(68, 'update-user', NULL, NULL),
(69, 'sort-users', NULL, NULL),
(70, 'destroy-user', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `typicms_permission_role`
--

CREATE TABLE `typicms_permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_permission_role`
--

INSERT INTO `typicms_permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1);

-- --------------------------------------------------------

--
-- Table structure for table `typicms_permission_user`
--

CREATE TABLE `typicms_permission_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `typicms_posts`
--

CREATE TABLE `typicms_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_posts`
--

INSERT INTO `typicms_posts` (`id`, `image`, `created_at`, `updated_at`) VALUES
(1, NULL, '2017-11-20 08:03:01', '2017-11-20 08:03:54');

-- --------------------------------------------------------

--
-- Table structure for table `typicms_post_translations`
--

CREATE TABLE `typicms_post_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `summary` text COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_post_translations`
--

INSERT INTO `typicms_post_translations` (`id`, `post_id`, `locale`, `status`, `title`, `slug`, `summary`, `body`, `created_at`, `updated_at`) VALUES
(1, 1, 'en', 1, 'Post Title', 'post-title', 'Hello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post title', '<p>Hello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post title</p>\r\n', '2017-11-20 08:03:01', '2017-11-20 08:03:01'),
(2, 1, 'np', 1, 'पाेस्ट टाइट', 'post-title', 'Hello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post title', '<p>Hello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post titleHello Post title</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>नेपाली</p>\r\n', '2017-11-20 08:03:01', '2017-11-20 08:03:54');

-- --------------------------------------------------------

--
-- Table structure for table `typicms_roles`
--

CREATE TABLE `typicms_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_roles`
--

INSERT INTO `typicms_roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'administrator', NULL, NULL),
(2, 'visitor', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `typicms_role_user`
--

CREATE TABLE `typicms_role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `typicms_settings`
--

CREATE TABLE `typicms_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'config',
  `key_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_settings`
--

INSERT INTO `typicms_settings` (`id`, `group_name`, `key_name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'config', 'webmaster_email', 'info@example.com', '2013-11-20 14:21:24', '2014-03-18 07:03:01'),
(2, 'config', 'google_analytics_code', '', '2013-11-20 14:21:24', '2017-11-18 08:21:30'),
(3, 'config', 'lang_chooser', '0', '2013-11-20 14:21:24', '2017-11-18 22:59:55'),
(4, 'fr', 'website_title', 'Site web sans titre', '2013-11-20 14:21:24', '2014-03-18 07:03:01'),
(5, 'fr', 'status', '1', '2013-11-20 14:21:24', '2014-03-18 07:03:01'),
(6, 'nl', 'website_title', 'Untitled website', '2013-11-20 14:21:24', '2014-03-18 07:03:01'),
(7, 'nl', 'status', '1', '2013-11-20 14:21:24', '2014-03-18 07:03:01'),
(8, 'en', 'website_title', 'Dhangadi Upamahanagarpalika', '2013-11-20 14:21:24', '2017-11-18 23:21:19'),
(9, 'en', 'status', '1', '2013-11-20 14:21:24', '2014-03-18 07:03:01'),
(10, 'es', 'website_title', 'Website sin título', '2013-11-20 14:21:24', '2014-03-18 07:03:01'),
(11, 'es', 'status', '1', '2013-11-20 14:21:24', '2014-03-18 07:03:01'),
(12, 'config', 'welcome_message', 'Welcome to the administration panel.', '2014-03-18 07:03:01', '2017-11-18 08:34:36'),
(13, 'config', 'auth_public', '0', '2014-03-18 07:03:01', '2014-03-18 07:03:01'),
(14, 'config', 'register', '0', '2014-03-18 07:03:01', '2014-03-18 07:03:01'),
(15, 'config', 'admin_locale', 'en', '2014-03-22 07:03:01', '2014-03-22 07:03:01'),
(16, 'config', '_token', '2fi42eXEIpMDddhnXGn7BdVcRJLjvz9BKobnF3bJ', '2017-11-18 08:21:30', '2017-11-18 22:51:32'),
(17, 'np', 'website_title', ' धनगढी उप-महानगरपालिका', '2017-11-18 08:21:30', '2017-11-18 23:18:58'),
(18, 'np', 'status', '1', '2017-11-18 08:21:30', '2017-11-18 08:25:01'),
(19, 'config', 'image', 'logo-01.png', '2017-11-18 08:21:30', '2017-11-18 08:36:16'),
(20, 'config', 'google_tag_manager_id', '', '2017-11-18 08:21:30', '2017-11-18 08:21:30');

-- --------------------------------------------------------

--
-- Table structure for table `typicms_translations`
--

CREATE TABLE `typicms_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `group` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_translations`
--

INSERT INTO `typicms_translations` (`id`, `group`, `key`, `created_at`, `updated_at`) VALUES
(1, 'db', 'More', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(2, 'db', 'Skip to content', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(4, 'db', 'languages.en', '2014-02-28 17:05:19', '2017-11-20 06:51:57'),
(5, 'db', 'languages.np', '2014-02-28 17:05:19', '2017-11-18 08:30:30'),
(7, 'db', 'Search', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(8, 'db', 'message when contact form is sent', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(9, 'db', 'message when errors in form', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(10, 'db', 'Add to calendar', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(11, 'db', 'All news', '2014-02-28 09:54:28', '2014-02-28 09:54:28'),
(12, 'db', 'All events', '2014-02-28 09:55:04', '2014-02-28 09:55:04'),
(13, 'db', 'Partners', '2014-02-28 09:55:35', '2014-02-28 09:55:35'),
(14, 'db', 'Latest news', '2014-02-28 09:56:21', '2014-02-28 09:56:21'),
(15, 'db', 'Incoming events', '2014-02-28 09:56:54', '2014-02-28 09:56:54'),
(16, 'db', 'Error :code', '2014-02-28 10:50:45', '2014-02-28 10:50:45'),
(17, 'db', 'Sorry, you are not authorized to view this page', '2014-02-28 10:53:43', '2014-02-28 10:53:43'),
(18, 'db', 'Go to our homepage?', '2014-02-28 10:55:40', '2014-02-28 10:55:40'),
(19, 'db', 'Sorry, this page was not found', '2014-02-28 10:57:18', '2014-02-28 10:57:18'),
(20, 'db', 'Sorry, a server error occurred', '2014-02-28 10:59:46', '2014-02-28 10:59:46'),
(21, 'db', 'Open navigation', '2016-02-15 10:59:46', '2016-02-15 10:59:46');

-- --------------------------------------------------------

--
-- Table structure for table `typicms_translation_translations`
--

CREATE TABLE `typicms_translation_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `translation_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `translation` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_translation_translations`
--

INSERT INTO `typicms_translation_translations` (`id`, `translation_id`, `locale`, `translation`, `created_at`, `updated_at`) VALUES
(1, 1, 'fr', 'En savoir plus', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(2, 1, 'en', 'More', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(3, 1, 'nl', 'Meer', '2014-02-28 17:05:51', '2014-02-28 17:05:51'),
(4, 1, 'es', 'Saber más', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(5, 2, 'fr', 'Aller au contenu', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(6, 2, 'en', 'Skip to content', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(7, 2, 'nl', 'Naar inhoud', '2014-02-28 17:05:51', '2014-02-28 17:05:51'),
(8, 2, 'es', 'Ir al contenido', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(13, 4, 'fr', 'English', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(14, 4, 'en', 'English', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(15, 4, 'nl', 'English', '2014-02-28 17:05:51', '2014-02-28 17:05:51'),
(16, 4, 'es', 'Inglés', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(17, 5, 'fr', 'Nederlands', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(18, 5, 'en', 'Nepali', '2014-02-28 17:05:19', '2017-11-18 08:30:29'),
(19, 5, 'nl', 'Nederlands', '2014-02-28 17:05:51', '2014-02-28 17:05:51'),
(20, 5, 'es', 'Holandés', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(25, 7, 'fr', 'Chercher', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(26, 7, 'en', 'Search', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(27, 7, 'nl', 'Zoeken', '2014-02-28 17:05:51', '2014-02-28 17:05:51'),
(28, 7, 'es', 'Buscar', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(29, 8, 'fr', 'Merci', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(30, 8, 'en', 'Thank you', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(31, 8, 'nl', 'Dank u', '2014-02-28 17:05:51', '2014-02-28 17:05:51'),
(32, 8, 'es', 'Gracias', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(33, 9, 'fr', 'Veuillez s’il vous plaît corriger les erreurs ci-dessous', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(34, 9, 'en', 'Please correct the errors below', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(35, 9, 'nl', 'Gelieve de onderstaande fouten te corrigeren', '2014-02-28 17:05:51', '2014-02-28 17:05:51'),
(36, 9, 'es', 'Por favor solucionado los siguientes errores', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(37, 10, 'fr', 'Ajouter au calendrier', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(38, 10, 'en', 'Add to calendar', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(39, 10, 'nl', 'Toevoegen aan Agenda', '2014-02-28 17:05:51', '2014-02-28 17:05:51'),
(40, 10, 'es', 'Añadir al calendario', '2014-02-28 17:05:19', '2014-02-28 17:05:19'),
(41, 11, 'fr', 'Toutes les actualités', '2014-02-28 09:54:28', '2014-02-28 09:54:28'),
(42, 11, 'nl', 'Alle nieuws', '2014-02-28 09:54:28', '2014-02-28 09:54:28'),
(43, 11, 'en', 'All news', '2014-02-28 09:54:28', '2014-02-28 09:54:28'),
(44, 11, 'es', 'Todas las noticias', '2014-02-28 09:54:28', '2014-02-28 09:54:28'),
(45, 12, 'fr', 'Tous les événements', '2014-02-28 09:55:04', '2014-02-28 09:55:04'),
(46, 12, 'nl', 'Alle evenementen', '2014-02-28 09:55:04', '2014-02-28 09:55:04'),
(47, 12, 'en', 'All events', '2014-02-28 09:55:04', '2014-02-28 09:55:04'),
(48, 12, 'es', 'Todos los eventos', '2014-02-28 09:55:04', '2014-02-28 09:55:04'),
(49, 13, 'fr', 'Partenaires', '2014-02-28 09:55:35', '2014-02-28 09:55:35'),
(50, 13, 'nl', 'Partners', '2014-02-28 09:55:35', '2014-02-28 09:55:35'),
(51, 13, 'en', 'Partners', '2014-02-28 09:55:35', '2014-02-28 09:55:35'),
(52, 13, 'es', 'Socios', '2014-02-28 09:55:35', '2014-02-28 09:55:35'),
(53, 14, 'fr', 'Dernières actualités', '2014-02-28 09:56:21', '2014-02-28 09:56:21'),
(54, 14, 'nl', 'Laatste Nieuws', '2014-02-28 09:56:21', '2014-02-28 09:56:21'),
(55, 14, 'en', 'Latest news', '2014-02-28 09:56:21', '2014-02-28 09:56:21'),
(56, 14, 'es', 'Últimas noticias', '2014-02-28 09:56:21', '2014-02-28 09:56:21'),
(57, 15, 'fr', 'Prochains événements', '2014-02-28 09:56:54', '2014-02-28 09:56:54'),
(58, 15, 'nl', 'Aankomende evenementen', '2014-02-28 09:56:54', '2014-02-28 09:56:54'),
(59, 15, 'en', 'Incoming events', '2014-02-28 09:56:54', '2014-02-28 09:56:54'),
(60, 15, 'es', 'Próximos eventos', '2014-02-28 09:56:54', '2014-02-28 09:56:54'),
(61, 16, 'fr', 'Erreur :code', '2014-02-28 10:50:45', '2014-02-28 10:50:45'),
(62, 16, 'nl', 'Error :code', '2014-02-28 10:50:45', '2014-02-28 10:50:45'),
(63, 16, 'en', 'Error :code', '2014-02-28 10:50:45', '2014-02-28 10:50:45'),
(64, 16, 'es', 'Error :code', '2014-02-28 10:50:45', '2014-02-28 10:50:45'),
(65, 17, 'fr', 'Désolé, vous n’êtes pas autorisé à voir cette page', '2014-02-28 10:53:43', '2014-02-28 10:53:43'),
(66, 17, 'nl', 'Sorry, u bent niet bevoegd om deze pagina te bekijken', '2014-02-28 10:53:43', '2014-02-28 10:53:43'),
(67, 17, 'en', 'Sorry, you are not authorized to view this page', '2014-02-28 10:53:43', '2014-02-28 10:53:43'),
(68, 17, 'es', 'Disculpe, usted no está autorizado a ver esta página', '2014-02-28 10:53:43', '2014-02-28 10:53:43'),
(69, 18, 'fr', 'Souhaitez-vous visiter notre :a_openpage d’accueil:a_close ?', '2014-02-28 10:55:40', '2014-02-28 10:55:40'),
(70, 18, 'nl', 'Wilt u onze :a_openhomepage:a_close te bezoeken?', '2014-02-28 10:55:40', '2014-02-28 10:55:40'),
(71, 18, 'en', 'Go to our :a_openhomepage:a_close?', '2014-02-28 10:55:40', '2014-02-28 10:55:40'),
(72, 18, 'es', 'Ir a la página de :a_openinicio:a_close ?', '2014-02-28 10:55:40', '2014-02-28 10:55:40'),
(73, 19, 'fr', 'Désolé, cette page n’a pas été trouvée', '2014-02-28 10:57:18', '2014-02-28 10:57:18'),
(74, 19, 'nl', 'Sorry, deze pagina is niet gevonden', '2014-02-28 10:57:18', '2014-02-28 10:57:18'),
(75, 19, 'en', 'Sorry, this page was not found', '2014-02-28 10:57:18', '2014-02-28 10:57:18'),
(76, 19, 'es', 'Disculpe, la página no ha sido encontrada', '2014-02-28 10:57:18', '2014-02-28 10:57:18'),
(77, 20, 'fr', 'Désolé, une erreur serveur est survenue', '2014-02-28 10:59:46', '2014-02-28 10:59:46'),
(78, 20, 'nl', 'Sorry, er is een serverfout opgetreden', '2014-02-28 10:59:46', '2014-02-28 10:59:46'),
(79, 20, 'en', 'Sorry, a server error occurred', '2014-02-28 10:59:46', '2014-02-28 10:59:46'),
(80, 20, 'es', 'Disculpe, ha ocurrido un error en el servidor', '2014-02-28 10:59:46', '2014-02-28 10:59:46'),
(81, 21, 'fr', 'Aller à la navigation', '2016-02-15 10:59:46', '2016-02-15 10:59:46'),
(82, 21, 'nl', 'Open navigatie', '2016-02-15 10:59:46', '2016-02-15 10:59:46'),
(83, 21, 'en', 'Open navigation', '2016-02-15 10:59:46', '2016-02-15 10:59:46'),
(84, 21, 'es', 'Ir a la navegación', '2016-02-15 10:59:46', '2016-02-15 10:59:46'),
(85, 5, 'np', 'नेपाली', '2017-11-18 08:30:30', '2017-11-18 08:30:30'),
(86, 4, 'np', 'English', '2017-11-20 06:51:57', '2017-11-20 06:51:57');

-- --------------------------------------------------------

--
-- Table structure for table `typicms_users`
--

CREATE TABLE `typicms_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `superuser` tinyint(1) NOT NULL DEFAULT '0',
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preferences` text COLLATE utf8_unicode_ci,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_users`
--

INSERT INTO `typicms_users` (`id`, `email`, `password`, `activated`, `superuser`, `first_name`, `last_name`, `preferences`, `token`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'me.umeshuser@gmail.com', '$2y$10$ZOSJ1P3jT6FEpK9z4GxvG.fkEhmTEb/C5et5VY5tUyXkVM3cze7DO', 1, 1, 'admin', 'admin', NULL, 'bXNR70dHHSVdNQoDxNTRb5o4QRzwGY', NULL, '2017-11-18 08:07:40', '2017-11-18 08:07:40');

-- --------------------------------------------------------

--
-- Table structure for table `typicms_widgets`
--

CREATE TABLE `typicms_widgets` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_widgets`
--

INSERT INTO `typicms_widgets` (`id`, `image`, `created_at`, `updated_at`) VALUES
(1, 'janawar.PNG', '2017-11-23 05:21:01', '2017-11-23 09:00:00'),
(2, NULL, '2017-11-23 09:00:59', '2017-11-23 09:01:00');

-- --------------------------------------------------------

--
-- Table structure for table `typicms_widget_translations`
--

CREATE TABLE `typicms_widget_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `widget_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `summary` text COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typicms_widget_translations`
--

INSERT INTO `typicms_widget_translations` (`id`, `widget_id`, `locale`, `status`, `title`, `slug`, `summary`, `body`, `created_at`, `updated_at`) VALUES
(1, 1, 'en', 1, 'This is Untitled', 'this-is-untitled', 'This is Untitled', '<p>This is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is UntitledThis is Untitled</p>\r\n', '2017-11-23 05:21:01', '2017-11-23 05:21:37'),
(2, 1, 'np', 1, 'दायाँ विजेट', 'rigth-widget', 'This is Right Widget', '<p>This is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right WidgetThis is Right Widget</p>\r\n', '2017-11-23 05:21:02', '2017-11-23 08:59:02'),
(3, 2, 'en', 1, 'Another Widgets', 'another-widgets', 'this-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitled', '<p>this-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitled</p>\r\n\r\n<p>this-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitled</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; this-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitled</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; this-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitled</p>\r\n\r\n<p>this-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitled</p>\r\n\r\n<p>this-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitledthis-is-untitled</p>\r\n', '2017-11-23 09:00:59', '2017-11-23 09:00:59'),
(4, 2, 'np', 0, '', NULL, '', '', '2017-11-23 09:01:00', '2017-11-23 09:01:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `typicms_blocks`
--
ALTER TABLE `typicms_blocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typicms_block_translations`
--
ALTER TABLE `typicms_block_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `block_translations_block_id_locale_unique` (`block_id`,`locale`);

--
-- Indexes for table `typicms_files`
--
ALTER TABLE `typicms_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `files_gallery_id_foreign` (`gallery_id`);

--
-- Indexes for table `typicms_file_translations`
--
ALTER TABLE `typicms_file_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `file_translations_file_id_locale_unique` (`file_id`,`locale`),
  ADD KEY `file_translations_locale_index` (`locale`);

--
-- Indexes for table `typicms_galleries`
--
ALTER TABLE `typicms_galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typicms_galleryables`
--
ALTER TABLE `typicms_galleryables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `galleryables_gallery_id_foreign` (`gallery_id`);

--
-- Indexes for table `typicms_gallery_translations`
--
ALTER TABLE `typicms_gallery_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `gallery_translations_gallery_id_locale_unique` (`gallery_id`,`locale`),
  ADD UNIQUE KEY `gallery_translations_locale_slug_unique` (`locale`,`slug`);

--
-- Indexes for table `typicms_history`
--
ALTER TABLE `typicms_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typicms_menulinks`
--
ALTER TABLE `typicms_menulinks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menulinks_menu_id_foreign` (`menu_id`),
  ADD KEY `menulinks_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `typicms_menulink_translations`
--
ALTER TABLE `typicms_menulink_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menulink_translations_menulink_id_locale_unique` (`menulink_id`,`locale`),
  ADD KEY `menulink_translations_locale_index` (`locale`);

--
-- Indexes for table `typicms_menus`
--
ALTER TABLE `typicms_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typicms_menu_translations`
--
ALTER TABLE `typicms_menu_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menu_translations_menu_id_locale_unique` (`menu_id`,`locale`),
  ADD KEY `menu_translations_locale_index` (`locale`);

--
-- Indexes for table `typicms_news`
--
ALTER TABLE `typicms_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typicms_news_translations`
--
ALTER TABLE `typicms_news_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `news_translations_news_id_locale_unique` (`news_id`,`locale`),
  ADD UNIQUE KEY `news_translations_locale_slug_unique` (`locale`,`slug`);

--
-- Indexes for table `typicms_pages`
--
ALTER TABLE `typicms_pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pages_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `typicms_page_translations`
--
ALTER TABLE `typicms_page_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `page_translations_page_id_locale_unique` (`page_id`,`locale`),
  ADD UNIQUE KEY `page_translations_locale_uri_unique` (`locale`,`uri`),
  ADD KEY `page_translations_locale_index` (`locale`);

--
-- Indexes for table `typicms_password_resets`
--
ALTER TABLE `typicms_password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `typicms_permissions`
--
ALTER TABLE `typicms_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `typicms_permission_role`
--
ALTER TABLE `typicms_permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `typicms_permission_user`
--
ALTER TABLE `typicms_permission_user`
  ADD PRIMARY KEY (`user_id`,`permission_id`),
  ADD KEY `permission_user_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `typicms_posts`
--
ALTER TABLE `typicms_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typicms_post_translations`
--
ALTER TABLE `typicms_post_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `post_translations_post_id_locale_unique` (`post_id`,`locale`),
  ADD UNIQUE KEY `post_translations_locale_slug_unique` (`locale`,`slug`);

--
-- Indexes for table `typicms_roles`
--
ALTER TABLE `typicms_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `typicms_role_user`
--
ALTER TABLE `typicms_role_user`
  ADD PRIMARY KEY (`role_id`,`user_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `typicms_settings`
--
ALTER TABLE `typicms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typicms_translations`
--
ALTER TABLE `typicms_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typicms_translation_translations`
--
ALTER TABLE `typicms_translation_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translation_translations_translation_id_locale_unique` (`translation_id`,`locale`),
  ADD KEY `translation_translations_locale_index` (`locale`);

--
-- Indexes for table `typicms_users`
--
ALTER TABLE `typicms_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `typicms_widgets`
--
ALTER TABLE `typicms_widgets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typicms_widget_translations`
--
ALTER TABLE `typicms_widget_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `widget_translations_widget_id_locale_unique` (`widget_id`,`locale`),
  ADD UNIQUE KEY `widget_translations_locale_slug_unique` (`locale`,`slug`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `typicms_blocks`
--
ALTER TABLE `typicms_blocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `typicms_block_translations`
--
ALTER TABLE `typicms_block_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `typicms_files`
--
ALTER TABLE `typicms_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `typicms_file_translations`
--
ALTER TABLE `typicms_file_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `typicms_galleries`
--
ALTER TABLE `typicms_galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `typicms_galleryables`
--
ALTER TABLE `typicms_galleryables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `typicms_gallery_translations`
--
ALTER TABLE `typicms_gallery_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `typicms_history`
--
ALTER TABLE `typicms_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `typicms_menulinks`
--
ALTER TABLE `typicms_menulinks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `typicms_menulink_translations`
--
ALTER TABLE `typicms_menulink_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `typicms_menus`
--
ALTER TABLE `typicms_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `typicms_menu_translations`
--
ALTER TABLE `typicms_menu_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `typicms_news`
--
ALTER TABLE `typicms_news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `typicms_news_translations`
--
ALTER TABLE `typicms_news_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `typicms_pages`
--
ALTER TABLE `typicms_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `typicms_page_translations`
--
ALTER TABLE `typicms_page_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `typicms_permissions`
--
ALTER TABLE `typicms_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `typicms_posts`
--
ALTER TABLE `typicms_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `typicms_post_translations`
--
ALTER TABLE `typicms_post_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `typicms_roles`
--
ALTER TABLE `typicms_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `typicms_settings`
--
ALTER TABLE `typicms_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `typicms_translations`
--
ALTER TABLE `typicms_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `typicms_translation_translations`
--
ALTER TABLE `typicms_translation_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT for table `typicms_users`
--
ALTER TABLE `typicms_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `typicms_widgets`
--
ALTER TABLE `typicms_widgets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `typicms_widget_translations`
--
ALTER TABLE `typicms_widget_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `typicms_block_translations`
--
ALTER TABLE `typicms_block_translations`
  ADD CONSTRAINT `block_translations_block_id_foreign` FOREIGN KEY (`block_id`) REFERENCES `typicms_blocks` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `typicms_files`
--
ALTER TABLE `typicms_files`
  ADD CONSTRAINT `files_gallery_id_foreign` FOREIGN KEY (`gallery_id`) REFERENCES `typicms_galleries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `typicms_file_translations`
--
ALTER TABLE `typicms_file_translations`
  ADD CONSTRAINT `file_translations_file_id_foreign` FOREIGN KEY (`file_id`) REFERENCES `typicms_files` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `typicms_galleryables`
--
ALTER TABLE `typicms_galleryables`
  ADD CONSTRAINT `galleryables_gallery_id_foreign` FOREIGN KEY (`gallery_id`) REFERENCES `typicms_galleries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `typicms_gallery_translations`
--
ALTER TABLE `typicms_gallery_translations`
  ADD CONSTRAINT `gallery_translations_gallery_id_foreign` FOREIGN KEY (`gallery_id`) REFERENCES `typicms_galleries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `typicms_menulinks`
--
ALTER TABLE `typicms_menulinks`
  ADD CONSTRAINT `menulinks_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `typicms_menus` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `menulinks_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `typicms_menulinks` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `typicms_menulink_translations`
--
ALTER TABLE `typicms_menulink_translations`
  ADD CONSTRAINT `menulink_translations_menulink_id_foreign` FOREIGN KEY (`menulink_id`) REFERENCES `typicms_menulinks` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `typicms_menu_translations`
--
ALTER TABLE `typicms_menu_translations`
  ADD CONSTRAINT `menu_translations_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `typicms_menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `typicms_news_translations`
--
ALTER TABLE `typicms_news_translations`
  ADD CONSTRAINT `news_translations_news_id_foreign` FOREIGN KEY (`news_id`) REFERENCES `typicms_news` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `typicms_pages`
--
ALTER TABLE `typicms_pages`
  ADD CONSTRAINT `pages_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `typicms_pages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `typicms_page_translations`
--
ALTER TABLE `typicms_page_translations`
  ADD CONSTRAINT `page_translations_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `typicms_pages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `typicms_permission_role`
--
ALTER TABLE `typicms_permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `typicms_permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `typicms_roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `typicms_permission_user`
--
ALTER TABLE `typicms_permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `typicms_permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `typicms_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `typicms_post_translations`
--
ALTER TABLE `typicms_post_translations`
  ADD CONSTRAINT `post_translations_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `typicms_posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `typicms_role_user`
--
ALTER TABLE `typicms_role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `typicms_roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `typicms_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `typicms_translation_translations`
--
ALTER TABLE `typicms_translation_translations`
  ADD CONSTRAINT `translation_translations_translation_id_foreign` FOREIGN KEY (`translation_id`) REFERENCES `typicms_translations` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `typicms_widget_translations`
--
ALTER TABLE `typicms_widget_translations`
  ADD CONSTRAINT `widget_translations_widget_id_foreign` FOREIGN KEY (`widget_id`) REFERENCES `typicms_widgets` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
