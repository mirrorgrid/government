@extends('core::admin.master')

@section('title', trans('posts::global.New'))

@section('main')

    @include('core::admin._button-back', ['module' => 'posts'])
    <h1>
        @lang('posts::global.New')
    </h1>

    {!! BootForm::open()->action(route('admin::index-posts'))->multipart()->role('form') !!}
        @include('posts::admin._form')
    {!! BootForm::close() !!}

@endsection
