<div class="col-md-2 section main-logo text-right">
    <a href="{{ TypiCMS::homeUrl() }}" title="Home" rel="home" id="logo">
        <img src="{{ url('uploads/settings/'.config('typicms.image')) }}" alt="{{ TypiCMS::title() }}">
    </a>
</div>
<div class="col-md-7 section main-logo">
    <div id="name-and-slogan">


        <h1 id="site-name">
            <a href="{{ TypiCMS::homeUrl() }}" title="{{ TypiCMS::title() }}"
               rel="home"><span> {{ TypiCMS::title() }}</span></a>
        </h1>

    </div> <!-- /#name-and-slogan -->

</div>
<div class="col-md-2 text-right pull-right">
    <div class="language-bar">
        <div class="sidebar-offcanvas">

            <button class="btn-offcanvas btn-offcanvas-close" data-toggle="offcanvas"
                    title="@lang('global.Close navigation')" aria-label="@lang('global.Close navigation')"><span
                        class="fa fa-close fa-fw" aria-hidden="true"></span></button>

            @section('lang-switcher')
                @include('core::public._lang-switcher')
            @show

        </div>
    </div>
    <div class="clearfix"></div>
</div>

