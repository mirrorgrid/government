@extends('core::admin.master')

@section('title', trans('widgets::global.New'))

@section('main')

    @include('core::admin._button-back', ['module' => 'widgets'])
    <h1>
        @lang('widgets::global.New')
    </h1>

    {!! BootForm::open()->action(route('admin::index-widgets'))->multipart()->role('form') !!}
        @include('widgets::admin._form')
    {!! BootForm::close() !!}

@endsection
