<?php

namespace TypiCMS\Modules\Posts\Http\Controllers;

use TypiCMS\Modules\Core\Http\Controllers\BasePublicController;
use TypiCMS\Modules\Posts\Repositories\PostInterface;

class PublicController extends BasePublicController
{
    public function __construct(PostInterface $post)
    {
        parent::__construct($post);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $models = $this->repository->all();

        return view('posts::public.index')
            ->with(compact('models'));
    }

    /**
     * Show news.
     *
     * @return \Illuminate\View\View
     */
    public function show($slug)
    {
        $model = $this->repository->bySlug($slug);

        return view('posts::public.show')
            ->with(compact('model'));
    }
}
