<?php

namespace TypiCMS\Modules\Posts\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Gate;
use Maatwebsite\Sidebar\SidebarGroup;
use Maatwebsite\Sidebar\SidebarItem;

class SidebarViewComposer
{
    public function compose(View $view)
    {
        $view->sidebar->group(trans('global.menus.content'), function (SidebarGroup $group) {
            $group->addItem(trans('posts::global.name'), function (SidebarItem $item) {
                $item->id = 'posts';
                $item->icon = config('typicms.posts.sidebar.icon');
                $item->weight = config('typicms.posts.sidebar.weight');
                $item->route('admin::index-posts');
                $item->append('admin::create-post');
                $item->authorize(
                    Gate::allows('index-posts')
                );
            });
        });
    }
}
