<?php

namespace TypiCMS\Modules\Posts\Repositories;

use Illuminate\Database\Eloquent\Model;
use TypiCMS\Modules\Core\Repositories\RepositoriesAbstract;

class EloquentPost extends RepositoriesAbstract implements PostInterface
{
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}
