<?php

namespace TypiCMS\Modules\Posts\Repositories;

use TypiCMS\Modules\Core\Repositories\RepositoryInterface;

interface PostInterface extends RepositoryInterface
{
}
