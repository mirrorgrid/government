<?php

namespace TypiCMS\Modules\Posts\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use TypiCMS\Modules\Core\Facades\TypiCMS;
use TypiCMS\Modules\Core\Observers\FileObserver;
use TypiCMS\Modules\Core\Observers\SlugObserver;
use TypiCMS\Modules\Core\Services\Cache\LaravelCache;
use TypiCMS\Modules\Posts\Models\Post;
use TypiCMS\Modules\Posts\Models\PostTranslation;
use TypiCMS\Modules\Posts\Repositories\CacheDecorator;
use TypiCMS\Modules\Posts\Repositories\EloquentPost;

class ModuleProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php', 'typicms.posts'
        );

        $modules = $this->app['config']['typicms']['modules'];
        $this->app['config']->set('typicms.modules', array_merge(['posts' => ['linkable_to_page']], $modules));

        $this->loadViewsFrom(__DIR__.'/../resources/views/', 'posts');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'posts');

        $this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/posts'),
        ], 'views');
        $this->publishes([
            __DIR__.'/../database' => base_path('database'),
        ], 'migrations');

        AliasLoader::getInstance()->alias(
            'Posts',
            'TypiCMS\Modules\Posts\Facades\Facade'
        );

        // Observers
        PostTranslation::observe(new SlugObserver());
        Post::observe(new FileObserver());
    }

    public function register()
    {
        $app = $this->app;

        /*
         * Register route service provider
         */
        $app->register('TypiCMS\Modules\Posts\Providers\RouteServiceProvider');

        /*
         * Sidebar view composer
         */
        $app->view->composer('core::admin._sidebar', 'TypiCMS\Modules\Posts\Composers\SidebarViewComposer');

        /*
         * Add the page in the view.
         */
        $app->view->composer('posts::public.*', function ($view) {
            $view->page = TypiCMS::getPageLinkedToModule('posts');
        });

        $app->bind('TypiCMS\Modules\Posts\Repositories\PostInterface', function (Application $app) {
            $repository = new EloquentPost(new Post());
            if (!config('typicms.cache')) {
                return $repository;
            }
            $laravelCache = new LaravelCache($app['cache'], 'posts', 10);

            return new CacheDecorator($repository, $laravelCache);
        });
    }
}
