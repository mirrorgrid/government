<?php

namespace TypiCMS\Modules\Widgets\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use TypiCMS\Modules\Core\Facades\TypiCMS;
use TypiCMS\Modules\Core\Observers\FileObserver;
use TypiCMS\Modules\Core\Observers\SlugObserver;
use TypiCMS\Modules\Core\Services\Cache\LaravelCache;
use TypiCMS\Modules\Widgets\Models\Widget;
use TypiCMS\Modules\Widgets\Models\WidgetTranslation;
use TypiCMS\Modules\Widgets\Repositories\CacheDecorator;
use TypiCMS\Modules\Widgets\Repositories\EloquentWidget;

class ModuleProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php', 'typicms.widgets'
        );

        $modules = $this->app['config']['typicms']['modules'];
        $this->app['config']->set('typicms.modules', array_merge(['widgets' => ['linkable_to_page']], $modules));

        $this->loadViewsFrom(__DIR__.'/../resources/views/', 'widgets');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'widgets');

        $this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/widgets'),
        ], 'views');
        $this->publishes([
            __DIR__.'/../database' => base_path('database'),
        ], 'migrations');

        AliasLoader::getInstance()->alias(
            'Widgets',
            'TypiCMS\Modules\Widgets\Facades\Facade'
        );

        // Observers
        WidgetTranslation::observe(new SlugObserver());
        Widget::observe(new FileObserver());
    }

    public function register()
    {
        $app = $this->app;

        /*
         * Register route service provider
         */
        $app->register('TypiCMS\Modules\Widgets\Providers\RouteServiceProvider');

        /*
         * Sidebar view composer
         */
        $app->view->composer('core::admin._sidebar', 'TypiCMS\Modules\Widgets\Composers\SidebarViewComposer');

        /*
         * Add the page in the view.
         */
        $app->view->composer('widgets::public.*', function ($view) {
            $view->page = TypiCMS::getPageLinkedToModule('widgets');
        });

        $app->bind('TypiCMS\Modules\Widgets\Repositories\WidgetInterface', function (Application $app) {
            $repository = new EloquentWidget(new Widget());
            if (!config('typicms.cache')) {
                return $repository;
            }
            $laravelCache = new LaravelCache($app['cache'], 'widgets', 10);

            return new CacheDecorator($repository, $laravelCache);
        });
    }
}
