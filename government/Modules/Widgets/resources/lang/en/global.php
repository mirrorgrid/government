<?php

return [
    'name'     => 'Widgets',
    'widgets'  => 'widget|widgets',
    'New'      => 'New widget',
    'Edit'     => 'Edit widget',
    'Back'     => 'Back to widgets',
];
