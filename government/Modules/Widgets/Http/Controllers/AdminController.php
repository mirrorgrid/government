<?php

namespace TypiCMS\Modules\Widgets\Http\Controllers;

use TypiCMS\Modules\Core\Http\Controllers\BaseAdminController;
use TypiCMS\Modules\Widgets\Http\Requests\FormRequest;
use TypiCMS\Modules\Widgets\Models\Widget;
use TypiCMS\Modules\Widgets\Repositories\WidgetInterface;

class AdminController extends BaseAdminController
{
    public function __construct(WidgetInterface $widget)
    {
        parent::__construct($widget);
    }

    /**
     * List models.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $models = $this->repository->all([], true);
        app('JavaScript')->put('models', $models);

        return view('widgets::admin.index');
    }

    /**
     * Create form for a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $model = $this->repository->getModel();

        return view('widgets::admin.create')
            ->with(compact('model'));
    }

    /**
     * Edit form for the specified resource.
     *
     * @param \TypiCMS\Modules\Widgets\Models\Widget $widget
     *
     * @return \Illuminate\View\View
     */
    public function edit(Widget $widget)
    {
        return view('widgets::admin.edit')
            ->with(['model' => $widget]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \TypiCMS\Modules\Widgets\Http\Requests\FormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FormRequest $request)
    {
        $widget = $this->repository->create($request->all());

        return $this->redirect($request, $widget);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \TypiCMS\Modules\Widgets\Models\Widget             $widget
     * @param \TypiCMS\Modules\Widgets\Http\Requests\FormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Widget $widget, FormRequest $request)
    {
        $this->repository->update($request->all());

        return $this->redirect($request, $widget);
    }
}
