<?php

namespace TypiCMS\Modules\Widgets\Http\Controllers;

use TypiCMS\Modules\Core\Http\Controllers\BasePublicController;
use TypiCMS\Modules\Widgets\Repositories\WidgetInterface;

class PublicController extends BasePublicController
{
    public function __construct(WidgetInterface $widget)
    {
        parent::__construct($widget);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $models = $this->repository->all();

        return view('widgets::public.index')
            ->with(compact('models'));
    }

    /**
     * Show news.
     *
     * @return \Illuminate\View\View
     */
    public function show($slug)
    {
        $model = $this->repository->bySlug($slug);

        return view('widgets::public.show')
            ->with(compact('model'));
    }
}
