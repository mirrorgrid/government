<?php

namespace Faker\Provider\nl_NL;

class Internet extends \Faker\Provider\Internet
{
    protected static $freeEmailDomain = array('gmail.com', 'hotmail.np', 'live.np', 'yahoo.np');
    protected static $tld = array('com', 'com', 'com', 'net', 'org', 'np', 'np', 'np');
}
