<li>
    <a href="{{ route($lang.'.news.slug', $news->slug) }}" title="{{ $news->title }}">
        {!! $news->title !!}
        {!! $news->present()->thumb(null, 200) !!}
    </a>
</li>
