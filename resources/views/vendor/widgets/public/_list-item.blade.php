<li>
    <a href="{{ route($lang.'.widgets.slug', $widget->slug) }}" title="{{ $widget->title }}">
        {!! $widget->title !!}
        {!! $widget->present()->thumb(null, 200) !!}
    </a>
</li>
