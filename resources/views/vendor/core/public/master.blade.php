<!doctype html>
<html lang="{{ config('app.locale') }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">

    <meta property="og:site_name" content="{{ $websiteTitle }}">
    <meta property="og:title" content="@yield('ogTitle')">
    <meta property="og:description" content="@yield('description')">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ URL::full() }}">
    <meta property="og:image" content="@yield('image')">

    <link href="{{ app()->isLocal() ? asset('css/public.css') : asset(elixir('css/public.css')) }}" rel="stylesheet">
    <link href="{{ app()->isLocal() ? asset('css/bootstrap.css') : asset(elixir('css/bootstrap.css')) }}"
          rel="stylesheet">

    <link href="{{ app()->isLocal() ? asset('css/government.css') : asset(elixir('css/government.css')) }}"
          rel="stylesheet">

    @include('core::public._feed-links')

    @yield('css')

    @include('core::public._google_analytics_code')

</head>

<body class="body-{{ $lang }} @yield('bodyClass') @if($navbar)has-navbar @endif">

@include('core::public._google_tag_manager_code')

@section('skip-links')
    <a href="#main" class="skip-to-content">@lang('db.Skip to content')</a>
    <a href="#site-nav" class="btn-offcanvas" data-toggle="offcanvas" title="@lang('db.Open navigation')"
       aria-label="@lang('db.Open navigation')" role="button" aria-controls="navigation" aria-expanded="false"><span
                class="fa fa-bars fa-fw" aria-hidden="true"></span></a>
@show

@include('core::_navbar')

<header class="without-secondary-menu" id="header">

    @section('site-header')
        <div class="container">
            <div class="row">

                @section('site-title')
                    @include('core::public._site-title')
                @show
            </div>
            <div id="main-menu" class="navigation col-md-12">
                @section('site-nav')
                    <nav class="site-nav" id="site-nav">
                        {!! Menus::render('main') !!}
                    </nav>
                @show
            </div>
        </div>
    @show

</header>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="block">
                    @include('core::public._alert')

                    @yield('main')
                </div>
            </div>
            <div class="col-sm-4 home-page-sidebar">
                <div class="block">

                    @yield('main')
                </div>
            </div>
        </div>

    </div>
</div>
@section('site-footer')
    <footer class="site-footer" id="footer-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="block">
                        {!! Menus::render('footer-1') !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    {!! Menus::render('footer-2') !!}
                </div>

                <div class="col-sm-3">
                    {!! Menus::render('footer-3') !!}
                </div>
                <div class="col-sm-3">
                    {!! Blocks::render('footer-4-block') !!}
                </div>
            </div>
        </div>
    </footer>
@show
<script src="@if(app()->environment('production')){{ asset(elixir('js/public/components.min.js')) }}@else{{ asset('js/public/components.min.js') }}@endif"></script>
<script src="@if(app()->environment('production')){{ asset(elixir('js/public/master.js')) }}@else{{ asset('js/public/master.js') }}@endif"></script>
@if (Request::input('preview'))
    <script src="{{ asset('js/public/previewmode.js') }}"></script>
@endif

@yield('js')

</body>

</html>
