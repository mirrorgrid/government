<li>
    <a href="{{ route($lang.'.posts.slug', $post->slug) }}" title="{{ $post->title }}">
        {!! $post->title !!}
        {!! $post->present()->thumb(null, 200) !!}
    </a>
</li>
